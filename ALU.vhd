--ALU Entity and architecture
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library FUNC1, FUNC2, FUNC3, FUNC4, MUX;
entity ALU is 
    Port ( 	A_IN 		: in 	STD_LOGIC_VECTOR (3 downto 0);	--4 bit input
			B_IN 		: in 	STD_LOGIC_VECTOR (3 downto 0);	-- 4 bit input 
			CONT_SIG 	: in 	STD_LOGIC_VECTOR (1 downto 0);	--2 bit input
			OUT_SIG_BEH : out	STD_LOGIC_VECTOR (3 downto 0)	--4 bit output 
			); 
end ALU;

architecture toplevel of ALU is
	--Funktion 1 Component
	component Func1 is 
    	Port ( 	A_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0);	--4 bit input
				B_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0);	-- 4 bit input 
				OUT_SIG_BEH : out  	STD_LOGIC_VECTOR (3 downto 0)	--4 bit output
				);  
	end component;

	--Funktion 2 Component
	component Func2 is 
    	Port ( 	A_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0); 	--4 bit input
				OUT_SIG_BEH : out  	STD_LOGIC_VECTOR (3 downto 0)	--4 bit output
			); 
	end component;

	--Funktion 3 Component
	component Func3 is 
    	Port ( 	A_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0); 	--4 bit input
				B_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0); 	--4 bit input 
				OUT_SIG_BEH : out  	STD_LOGIC_VECTOR (3 downto 0)	--4 bit output
				); 
	end component;

	--Funktion 4 Component
	component Func4 is 
    	Port ( 	A_IN 		: in  	STD_LOGIC_VECTOR 	(3 downto 0); 	--4 bit input
				OUT_SIG_BEH : out  	STD_LOGIC_VECTOR 	(3 downto 0)	--4 bit output
				); 
	end component;

	--Mux component
	component Mux is 
    	Port ( 	F1_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0); 	--4 bit input
   		 		F2_IN 		: in 	STD_LOGIC_VECTOR (3 downto 0); 	--4 bit input
    			F3_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0); 	--4 bit input
    			F4_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0); 	--4 bit input
				CONT_SIG 	: in  	STD_LOGIC_VECTOR (1 downto 0); 	--1 bit input 
				OUT_SIG_BEH : out  	STD_LOGIC_VECTOR (3 downto 0)	--4 bit output
				);
	end component;

	--Signals for output
	signal F1, F2, F3, F4 	:	std_logic_vector(3 downto 0); --4 bit

	--Structural description of ALU
	begin 
		F1 		:	 entity FUNC1	port map (A_IN, B_IN, F1);
		F2 		:	 entity FUNC2 	port map (A_IN, F2);
		F3 		:	 entity FUNC3 	port map (A_IN, B_IN, F3);
		F4 		:	 entity FUNC4 	port map (A_IN, F4);
		MUX		:	 entity MUX		port map (F1, F2, F3, F4, CONT_SIG, OUT_SIG_BEH);
	
end toplevel;
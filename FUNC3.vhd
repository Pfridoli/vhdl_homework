--Function 3 Entity and architecture  : xor A, B (inverteerida sõna A B-nda biti väärtus)
library IEEE;
use IEEE.std_logic_1164.all;

entity Func3 is
  	Port ( 	A_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0);	--4 bit input
			B_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0);	--4 bit input 
			OUT_SIG_BEH : out  	STD_LOGIC_VECTOR (3 downto 0)	--4 bit output
			);
end Func3;

architecture FUNC3 of Func3 is
	begin
		FUNC3: process(A_IN, B_IN)
		begin
			if B_IN="0000" or B_IN="0100" or B_IN="1100" or B_IN="1000" then
				OUT_SIG_BEH <= A_IN(3 downto 1) & not(A_IN(0));
			elsif B_IN="0001" or B_IN="0101" or B_IN="1101" or B_IN="1001" then
				OUT_SIG_BEH <= A_IN(3 downto 2) & not(A_IN(1)) & A_IN(0);
			elsif B_IN="0010" or B_IN="0110" or B_IN="1110" or B_IN="1010" then
				OUT_SIG_BEH <= A_IN(3) & not(A_IN(2)) & A_IN(1 downto 0);
			elsif B_IN="0011" or B_IN="0111" or B_IN="1111" or B_IN="1011" then
				OUT_SIG_BEH <= not(A_IN(3)) & A_IN(2 downto 0);
			else
				OUT_SIG_BEH <= "XXXX";
			end if;
		end process FUNC3;
end FUNC3;
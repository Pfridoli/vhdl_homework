--Function 1 Entity and architecture : A – B (aritmeetiline lahutamine)
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity Func1 is
  	Port ( 	A_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0);	--4 bit input
			B_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0);	--4 bit input 
			OUT_SIG_BEH : out  	STD_LOGIC_VECTOR (3 downto 0)	--4 bit output
			);
end Func1;

architecture FUNC1 of Func1 is
	begin
		OUT_SIG_BEH <= unsigned(A_IN) - unsigned(B_IN);
end FUNC1;
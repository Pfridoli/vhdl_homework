--4:1 4-bit MUX Entity and architecture
library IEEE;
use IEEE.std_logic_1164.all;
entity Mux is
	port(
		F1_IN		:	in std_logic_vector	(3 downto 0);
		F2_IN		:	in std_logic_vector	(3 downto 0);
		F3_IN		:	in std_logic_vector	(3 downto 0);
		F4_IN		:	in std_logic_vector	(3 downto 0);
		CONT_SIG	:	in std_logic_vector	(1 downto 0);
		OUT_SIG_BEH	:	out std_logic_vector(3 downto 0)
		);
end Mux;

architecture MUX of Mux is
begin
	MUX: process(F1_IN,F2_IN,F3_IN,F4_IN, CONT_SIG)
		begin
			if CONT_SIG="00" then
				OUT_SIG_BEH <= F1_IN;
			elsif CONT_SIG="01" then
				OUT_SIG_BEH <= F2_IN;
			elsif CONT_SIG="10" then
				OUT_SIG_BEH <= F3_IN;
			elsif CONT_SIG="11" then
				OUT_SIG_BEH <= F4_IN;
			else
				OUT_SIG_BEH <= "XXXX";
			end if;	
	end process MUX;
end MUX;
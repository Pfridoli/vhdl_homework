--Function 4 Entity and architecture : kodutöö (vaata enda tõeväärtustabelit!!!)
library IEEE;
use IEEE.std_logic_1164.all;

entity Func4 is
  	Port ( 	A_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0);	--4 bit input
			OUT_SIG_BEH : out  	STD_LOGIC_VECTOR (3 downto 0)	--4 bit output
			);
end Func4;

architecture FUNC4 of Func4 is
	begin
		FUNC4: process(A_IN)
		begin
			if A_IN="0011" or A_IN="0100" or A_IN="0101" or A_IN="0111" or A_IN="1001" or A_IN="1111" then
				OUT_SIG_BEH <= "0001";
			else
				OUT_SIG_BEH <= "0000";
			end if;	
		end process FUNC4;
end FUNC4;
--Function 2 Entity and architecture : shr A (nihe paremale)
library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.numeric_std.all; 

entity Func2 is
  	Port ( 	A_IN 		: in  	STD_LOGIC_VECTOR (3 downto 0);	--4 bit input
			OUT_SIG_BEH : out  	STD_LOGIC_VECTOR (3 downto 0)	--4 bit output
			);
end Func2;

architecture FUNC2 of Func2 is
	begin
		OUT_SIG_BEH(0) <= A_IN(1);
		OUT_SIG_BEH(1) <= A_IN(2);
		OUT_SIG_BEH(2) <= A_IN(3);
		OUT_SIG_BEH(3) <= '0';
		--OUT_SIG_BEH <= std_logic_vector(shift_right(unsigned(A_IN), 1));
end FUNC2;
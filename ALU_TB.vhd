LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
 
entity ALU_TB is
end ALU_TB;

architecture testbench of ALU_TB is 
    -- Component Declaration for the Unit Under Test (UUT)
	component Alu is
    Port ( 	A_IN 		: in 	STD_LOGIC_VECTOR (3 downto 0);	--4 bit input
			B_IN 		: in 	STD_LOGIC_VECTOR (3 downto 0);	--4 bit input 
			CONT_SIG 	: in 	STD_LOGIC_VECTOR (1 downto 0);	--2 bit input
			OUT_SIG_BEH : out	STD_LOGIC_VECTOR (3 downto 0)	--4 bit output 
			); 
	end component;
    
	--Signal inputs and outputs from the testbench
	signal A_TB : std_logic_vector(3 downto 0);
	signal B_TB : std_logic_vector(3 downto 0);
	signal CONT_SIG_TB : std_logic_vector(1 downto 0);
	signal OUT_SIG_BEH_TB : std_logic_vector(3 downto 0);
	signal ANSWER : std_logic_vector(3 downto 0);

	begin
	--component port map
	ALU: Alu port map (
		A_IN => A_TB,
		B_IN => B_TB,
		CONT_SIG => CONT_SIG_TB,
		OUT_SIG_BEH => OUT_SIG_BEH_TB
	); 
	
	Stimulus: process
	begin
	--for debugging
	report "Test case 1";
	CONT_SIG_TB <= "00";
	
	A_TB <= "0000";
	B_TB <= "0001";
	ANSWER <= "0000"; 
	wait for 10 ns;
	
	A_TB <= "0100";
	B_TB <= "0010";
	ANSWER <= "0010";
	wait for 10 ns;
	
	A_TB <= "1000";
	B_TB <= "0100";
	ANSWER <= "0100";
	wait for 10 ns;
	
	A_TB <= "1000";
	B_TB <= "0001";
	ANSWER <= "0111";
	wait for 10 ns;
	

	report "Test case 2";
	B_TB <= "0000";
	CONT_SIG_TB <= "01";
	
	A_TB <= "1001";
	ANSWER <= "0100";
	wait for 10 ns;
	
	A_TB <= "0100";
	ANSWER <= "0010";
	wait for 10 ns;
	
	A_TB <= "1010";
	ANSWER <= "0101";
	wait for 10 ns;
	
	A_TB <= "1011";
	ANSWER <= "0101";
	wait for 10 ns;
	
	
	report "Test case 3";
	CONT_SIG_TB <= "10";
	
	B_TB <= "1100";
	A_TB <= "0010";
	ANSWER <= "0011";
	wait for 10 ns;
	
	B_TB <= "0001";
	A_TB <= "0111";
	ANSWER <= "0101";
	wait for 10 ns;
	
	B_TB <= "0010";
	A_TB <= "0001";
	ANSWER <= "0101";
	wait for 10 ns;
	
	B_TB <= "0011";
	A_TB <= "0111";
	ANSWER <= "1111";
	wait for 10 ns;
	
	
	report "Test case 4";
	B_TB <= "0000";
	CONT_SIG_TB <= "11";
	
	A_TB <= "0011";
	ANSWER <= "0001";
	wait for 10 ns;
	
	A_TB <= "0110";
	ANSWER <= "0000";
	wait for 10 ns;
	
	A_TB <= "1001";
	ANSWER <= "0001";
	wait for 10 ns;
	
	A_TB <= "1110";
	ANSWER <= "0000";
	wait for 10 ns;
	
	wait;
end process;

end testbench;